package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  test("demux example") {
    val in = new Wire
    val s = List[Wire](new Wire, new Wire)
    val f = List[Wire](new Wire, new Wire, new Wire, new Wire)
    demux(in, s, f)
    in.setSignal(true)
    s(0).setSignal(false)
    s(1).setSignal(false)
    run
    
    assert(f(0).getSignal === false, "f0: S1 0, S2 0")
    assert(f(1).getSignal === false, "f1: S1 0, S2 0")
    assert(f(2).getSignal === false, "f2: S1 0, S2 0")
    assert(f(3).getSignal === true,  "f3: S1 0, S2 0")
    
    s(0).setSignal(true)
    run
    assert(f(0).getSignal === false, "f0: S1 1, S2 0")
    assert(f(1).getSignal === true,  "f1: S1 1, S2 0")
    assert(f(2).getSignal === false, "f2: S1 1, S2 0")
    assert(f(3).getSignal === false, "f3: S1 1, S2 0")
    
    s(0).setSignal(false)
    s(1).setSignal(true)
    run
    assert(f(0).getSignal === false, "f0: S1 0, S2 1")
    assert(f(1).getSignal === false, "f1: S1 0, S2 1")
    assert(f(2).getSignal === true,  "f2: S1 0, S2 1")
    assert(f(3).getSignal === false, "f3: S1 0, S2 1")
    
    s(0).setSignal(true)
    s(1).setSignal(true)
    run
    assert(f(0).getSignal === true,  "f0: S1 1, S2 1")
    assert(f(1).getSignal === false, "f1: S1 1, S2 1")
    assert(f(2).getSignal === false, "f2: S1 1, S2 1")
    assert(f(3).getSignal === false, "f3: S1 1, S2 1")
  }
  
  test("or test"){
    val in1, in2, out = new Wire
    orGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  test("or2 test"){
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  
}
