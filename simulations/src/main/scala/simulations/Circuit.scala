package simulations

import common._

class Wire {
  private var sigVal = false
  private var actions: List[Simulator#Action] = List()

  def getSignal: Boolean = sigVal
  
  def setSignal(s: Boolean) {
    if (s != sigVal) {
      sigVal = s
      actions.foreach(action => action())
    }
  }

  def addAction(a: Simulator#Action) {
    actions = a :: actions
    a()
  }
}

abstract class CircuitSimulator extends Simulator {

  val InverterDelay: Int
  val AndGateDelay: Int
  val OrGateDelay: Int

  def probe(name: String, wire: Wire) {
    wire addAction {
      () => afterDelay(0) {
        println(
          "  " + currentTime + ": " + name + " -> " +  wire.getSignal)
      }
    }
  }

  def inverter(input: Wire, output: Wire) {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output.setSignal(!inputSig) }
    }
    input addAction invertAction
  }

  def andGate(a1: Wire, a2: Wire, output: Wire) {
    def andAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) { output.setSignal(a1Sig & a2Sig) }
    }
    a1 addAction andAction
    a2 addAction andAction
  }

  //
  // to complete with orGates and demux...
  //

  def orGate(a1: Wire, a2: Wire, output: Wire) {
    def orAction(){
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(OrGateDelay){output.setSignal(a1Sig | a2Sig)}
    }
    a1 addAction orAction
    a2 addAction orAction
  }
  
  def orGate2(a: Wire, b: Wire, output: Wire) {
    def orAction(){
      val notA, notB, notOut = new Wire
      inverter(a, notA)
      inverter(b, notB)
      andGate(notA, notB, notOut)
      inverter(notOut, output)
    }
    a addAction orAction
    b addAction orAction
  }

  def demux(in: Wire, c: List[Wire], out: List[Wire]) {
    c match {
      case Nil => andGate(in, in, out(0))
      case x::xs => {
        val inL, inR, notX = new Wire
        andGate(x, in, inL)
        inverter(x, notX)
        andGate(notX, in, inR)
        
        val n = out.length / 2
        demux(inL, xs, out take n)
        demux(inR, xs, out drop n)
      }
    }
  }

}

object Circuit extends CircuitSimulator {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5

  def andGateExample {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  def orGate2Example {
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }
  
  def demuxExample {
    val in = new Wire
    val s = List[Wire](new Wire, new Wire)
    val f = List[Wire](new Wire, new Wire, new Wire, new Wire)
    demux(in, s, f)
    probe("in", in)
    probe("f0", f(0))
    probe("f1", f(1))
    probe("f2", f(2))
    probe("f3", f(3))
    in.setSignal(true)
    s(0).setSignal(false)
    s(1).setSignal(false)
    run
    
    s(0).setSignal(true)
    run
    
    s(0).setSignal(false)
    s(1).setSignal(true)
    run
    
    s(0).setSignal(true)
    s(1).setSignal(true)
    run
  }

  //
  // to complete with orGateExample and demuxExample...
  //
}

object CircuitMain extends App {
  // You can write tests either here, or better in the test class CircuitSuite.
  Circuit.andGateExample
  
  println("orGate2 example \n")
  
  Circuit.orGate2Example
  
  println("demux example \n")
  
  Circuit.demuxExample
}
