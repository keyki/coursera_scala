package kvstore

import akka.actor._
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy._
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import akka.actor.Terminated
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.PoisonPill
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.util.Timeout
import scala.collection.SortedSet
import akka.event.LoggingReceive

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))

  val PERSISTENCE_INTERVAL = 50 millis span
  val TIMEOUT = 1 second span
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor with ActorLogging {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  var expectedSnapshotSequence: Long = 0

  val persistence: ActorRef = context.actorOf(persistenceProps)

  var persistencePrimaryAcks = Map.empty[Long, (ActorRef, String, Cancellable)]
  var persistenceAcks = Map.empty[Long, (ActorRef, String, Cancellable)]

  var waitings = Map.empty[Long, (ActorRef, Set[ActorRef])]

  var newReplicaAcks = SortedSet.empty[Long]

  override def preStart() {
    arbiter ! Join
    context.watch(persistence)
  }

  def receive = {
    case JoinedPrimary => context.become(LoggingReceive(leader))
    case JoinedSecondary => context.become(LoggingReceive(replica))
  }

  override val supervisorStrategy = OneForOneStrategy() {
    case e: PersistenceException => {
      waitings foreach {
        case (key, (sender, _)) => {
          sender ! OperationAck(key)
          waitings -= key
        }
      }
      persistenceAcks foreach {
        case (id, (sender, key, cancellable)) => {
          sender ! SnapshotAck(key, id)
          cancellable.cancel()
          persistenceAcks -= id
        }
      }
      Restart
    }
  }

  /* TODO Behavior for  the leader role. */
  val leader: Receive = {
    case Insert(key: String, value: String, id: Long) => {
      kv += (key -> value)
      replicators.foreach(_ ! Replicate(key, Some(value), id))
      val persistMessage = Persist(key, Some(value), id)
      val cancellable = context.system.scheduler.schedule(Duration.Zero, PERSISTENCE_INTERVAL, persistence, persistMessage)
      persistencePrimaryAcks += id -> (sender, key, cancellable)
      waitings += id -> (sender, replicators)
      scheduleTimeout(sender, id)
    }
    case Remove(key: String, id: Long) => {
      kv -= key
      replicators.foreach(_ ! Replicate(key, None, id))
      val persistMessage = Persist(key, None, id)
      val cancellable = context.system.scheduler.schedule(Duration.Zero, PERSISTENCE_INTERVAL, persistence, persistMessage)
      persistencePrimaryAcks += id -> (sender, key, cancellable)
      waitings += id -> (sender, replicators)
      scheduleTimeout(sender, id)
    }
    case Get(key: String, id: Long) =>
      sender ! GetResult(key, kv.get(key), id)
    case Replicas(replicas) => {
      replicas.filterNot(secondaries.contains).filter { _ != self }.foreach { replica =>
        val replicator = context.actorOf(Replicator.props(replica))
        secondaries += replica -> replicator
        replicators += replicator
        kv foreach {
          case (key, value) => {
            val i = if (newReplicaAcks.isEmpty) 5000 else newReplicaAcks.lastKey + 1
            //log.info(s"Replicate($key, Some($value), $i)")
            replicator ! Replicate(key, Some(value), i)
            newReplicaAcks += i
          }
        }
      }
      secondaries.keys filterNot replicas.contains foreach { removedReplica =>

        secondaries.get(removedReplica) match {
          case Some(replicator) => {
            waitings foreach {
              case (id, (sender, pending)) => {
                pending contains replicator match {
                  case true => {
                    val remainig = pending - replicator
                    remainig isEmpty match {
                      case true => {
                        waitings -= id
                        sender ! OperationAck(id)
                      }
                      case false => waitings += id -> (sender, remainig)
                    }
                  }
                  case false =>
                }
              }
            }
          }
          case None =>
        }

        secondaries(removedReplica) ! PoisonPill
        secondaries -= removedReplica
      }

    }
    case Persisted(key, seq) => {
      persistencePrimaryAcks.get(seq) match {
        case Some((sender,_, cancellable)) => {
          cancellable.cancel()
          persistencePrimaryAcks -= seq
          checkWaitings(seq, sender)
        }
        case None =>
      }
    }
    case Replicated(key, id) => {
      checkWaitings(id, sender)
    }
    case Terminated => log.info("receive exception")
  }

  /* TODO Behavior for the replica role. */
  val replica: Receive = {
    case Get(key: String, id: Long) =>
      sender ! GetResult(key, kv.get(key), id)
    case Snapshot(key, valueOption, seq) => {
      if (expectedSnapshotSequence > seq)
        sender ! SnapshotAck(key, seq)
      else if (expectedSnapshotSequence == seq) {
        expectedSnapshotSequence = seq + 1
        valueOption match {
          case None => kv -= key
          case Some(value) => kv += key -> value
        }
        val persistMessage = Persist(key, valueOption, seq)
        val cancellable = context.system.scheduler.schedule(Duration.Zero, PERSISTENCE_INTERVAL, persistence, persistMessage)
        persistenceAcks += seq -> (sender, key, cancellable)
      }
    }
    case Persisted(key, seq) => {
      persistenceAcks.get(seq) match {
        case Some((sender, _, cancellable)) => {
          cancellable.cancel()
          persistenceAcks -= seq
          sender ! SnapshotAck(key, seq)
        }
        case None =>
      }
    }
  }

  private def scheduleTimeout(_sender: ActorRef, id: Long) = {
    context.system.scheduler.scheduleOnce(1 second) {
      waitings get (id) match {
        case Some((sender, cancellable)) => {
          sender ! OperationFailed(id)
        }
        case None =>
      }
    }
  }

  private def checkWaitings(id: Long, responded: ActorRef) = {
    waitings.get(id) match {
      case Some((sender, waiting)) => {
        val remaining = waiting - responded
        remaining.isEmpty match {
          case true => {
            waitings -= id
            sender ! OperationAck(id)
          }
          case false => waitings += id -> (sender, remaining)
        }
      }
      case None => persistencePrimaryAcks = persistencePrimaryAcks.filter(x => x == id)
    }
  }

}
