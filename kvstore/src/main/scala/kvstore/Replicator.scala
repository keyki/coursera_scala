package kvstore

import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.ActorLogging
import scala.concurrent.duration._
import akka.event.LoggingReceive
import akka.actor.Cancellable

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)

  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor with ActorLogging {
  import Replicator._
  import Replica._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Snapshot, Cancellable)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]

  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  override def preStart() {
    context.watch(replica)
  }

  /*override def postStop() {
    acks.values foreach {
      case (actor, snapshot, cancellable) =>
        cancellable.cancel()
        //actor ! Replicated(snapshot.key, snapshot.seq)
    }
  }*/

  /* TODO Behavior for the Replicator. */
  def receive: Receive = LoggingReceive {
    case Replicate(key, valueOption, id) => {
      //log.info(s"replicator: Replicated($key, $valueOption, $id)")
      pending.find(x => x.key == key) match {
        case Some(snapshot) => acks.get(snapshot.seq) match {
          case Some((actor, snapshot, cancellable)) => {
            acks -= snapshot.seq
            pending = pending.filter(x => x.key == key)
            //log.info(s"replicator: Replicated($key, ${snapshot.seq})")
            actor ! Replicated(key, snapshot.seq)
            cancellable.cancel()
          }
          case None =>
        }
        case None =>
      }
      val nextInSeq = nextSeq
      val snapshotMessage = Snapshot(key, valueOption, nextInSeq)
      pending = pending :+ snapshotMessage
      val cancellable = context.system.scheduler.schedule(Duration.Zero, 100 millis span, replica, snapshotMessage)
      acks += nextInSeq -> (sender, snapshotMessage, cancellable)
    }
    case SnapshotAck(key, id) => {
      //log.info(s"replicator: SnapshotAck($key, $id)")
      acks.get(id) match {
        case Some((actor, snapshot, cancellable)) => {
          acks -= id
          actor ! Replicated(key, id)
          cancellable.cancel()
          pending = pending.filter(x => x.key == key)
        }
        case None =>
      }
    }
  }

}
