package quickcheck

import common._
import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("hint1") = forAll { (a: A, b: A) =>
    val h = insert(a, insert(b, empty))
    findMin(h) == Math.min(a, b)
  }
  
  property("hint2") = forAll { a: A =>
    val h = deleteMin(insert(a, empty))
    isEmpty(h)
  }
  
  property("hint3") = forAll { h: H =>
  	def isSorted(h: H): Boolean = {
  			if (isEmpty(h)) true
  			val m = findMin(h)
  			val hs = deleteMin(h)
  			isEmpty(hs) || (m <= findMin(hs) && isSorted(hs))
  	  }
  	isSorted(h)
  }
  
  property("hint4") = forAll { (h1 : H, h2: H) =>
    findMin(meld(h1, h2)) == Math.min(findMin(h1), findMin(h2)) 
  }
  
  property("gen1") = forAll { (h: H) =>
  	val m = if (isEmpty(h)) 0 else findMin(h)
  	findMin(insert(m, h))==m
  }
  
  property("meld") = forAll { (h1: H, h2: H) =>
    def heapEqual(h1: H, h2: H): Boolean =
      if (isEmpty(h1) && isEmpty(h2)) true
      else {
        val m1 = findMin(h1)
        val m2 = findMin(h2)
        m1 == m2 && heapEqual(deleteMin(h1), deleteMin(h2))
      }
    heapEqual(meld(h1, h2),
              meld(deleteMin(h1), insert(findMin(h1), h2)))
  }
  
  lazy val genHeap: Gen[H] = for {
    n <-arbitrary[A]
    h <- frequency((1, value(empty)), (9, genHeap))
  } yield insert(n, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
