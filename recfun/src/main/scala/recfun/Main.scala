package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
     println(countChange(4,List(1,2)))
    }
  }

  /**
   * Exercise 1
   * c - column
   * r - row
   * Return value of element in Pascal's Triangle
   */
  def pascal(c: Int, r: Int): Int = 
  {
    if (r == 0) 1
    else if (c == 0) 1
    else if (c == r) 1
    else pascal(c, r - 1) + pascal(c - 1, r - 1) 
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = 
  {
    def countParentheses(acc:Int, char: Char, chars: List[Char]): Int =
    {
      if (acc < 0) acc // closed parenthese without opening one
      else if (char.equals('(')) 
        if (!chars.isEmpty)
          countParentheses(acc + 1, chars.head, chars.tail)
        else acc + 1
      else if (char.equals(')')) 
        if (!chars.isEmpty)
          countParentheses(acc - 1, chars.head, chars.tail)
        else acc - 1
      else if (chars.isEmpty) acc
      else countParentheses(acc, chars.head, chars.tail)
    }
    countParentheses(0, chars.head, chars.tail) == 0
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = 
  {
    if (money == 0) 1
    else if (money < 0) 0
    else if (coins.isEmpty) 0
    else countChange(money, coins.tail) + countChange(money - coins.head, coins)
  }
}
