/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import akka.event._
import scala.collection.immutable.Queue
import scala.language.postfixOps

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
    
    override def toString() = s"${asInstanceOf[AnyRef].getClass.getSimpleName} ${id}, ${elem}"
  }

  trait OperationReply {
    def id: Int
  }

  /**
   * Request with identifier `id` to insert an element `elem` into the tree.
   * The actor at reference `requester` should be notified when this operation
   * is completed.
   */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /**
   * Request with identifier `id` to check whether an element `elem` is present
   * in the tree. The actor at reference `requester` should be notified when
   * this operation is completed.
   */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /**
   * Request with identifier `id` to remove the element `elem` from the tree.
   * The actor at reference `requester` should be notified when this operation
   * is completed.
   */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /**
   * Holds the answer to the Contains request with identifier `id`.
   * `result` is true if and only if the element is present in the tree.
   */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}

class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  //val log = Logging(context.system, this)

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = LoggingReceive {
    case o: Operation => 
      //log.info(s"${o.asInstanceOf[AnyRef].getClass.getSimpleName}: ${o.id}, ${o.elem}")
      root ! o
    case GC => {
      val newRoot = createRoot
      context become garbageCollecting(newRoot)
      root ! CopyTo(newRoot)
    }
  }

  // optional
  /**
   * Handles messages while garbage collection is performed.
   * `newRoot` is the root of the new binary tree where we want to copy
   * all non-removed elements into.
   */
  def garbageCollecting(newRoot: ActorRef): Receive = LoggingReceive {
    case o: Operation => 
      //log.info(s"GC: ${o.asInstanceOf[AnyRef].getClass.getSimpleName}: ${o.id}, ${o.elem}")
      pendingQueue = pendingQueue enqueue o
      //log.info(pendingQueue.mkString("; "))
    case CopyFinished => {
      context.stop(root)
      root = newRoot
      while (!pendingQueue.isEmpty) pendingQueue.dequeue match {
        case (record, queue) =>
          //log.info(s"GC send ${record}")
          newRoot ! record
          pendingQueue = queue
      }
      context unbecome
    }
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  //val log = Logging(context.system, this)
  
  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = LoggingReceive {
    case Insert(requester, id, elem) => insert(requester, id, elem)
    case Contains(requester, id, elem) => contains(requester, id, elem)
    case Remove(requester, id, elem) => remove(requester, id, elem)
    case CopyTo(node) => {
      val expected = subtrees.values.toSet
      context become copying(expected, !removed)
      if (!removed)
        node ! Insert(self, elem, elem)
      else
        self ! OperationFinished(elem)

      expected.foreach(_ ! CopyTo(node))
    }
  }

  // optional
  /**
   * `expected` is the set of ActorRefs whose replies we are waiting for,
   * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
   */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case OperationFinished(elem) =>
      context become copying(expected, insertConfirmed)
      if (expected.isEmpty)
        self ! CopyFinished

    case CopyFinished =>
      val left = expected - sender
      //log.info(s"CopyFinished ${left.size}, ${insertConfirmed}")
      if (left.isEmpty)
        context.parent ! CopyFinished
      else
        context become copying(left, insertConfirmed)
  }

  private def insert(requester: ActorRef, id: Int, e: Int): Unit = {
    if (e == elem) {
      if (removed)
        removed = false
      requester ! OperationFinished(id)
    } else {
      val position = positionOf(e)
      subtrees.get(position).map(_ ! Insert(requester, id, e)).getOrElse {
        subtrees += (position -> context.actorOf(props(e, false)))
        requester ! OperationFinished(id)
      }
    }
  }

  private def contains(requester: ActorRef, id: Int, e: Int): Unit = {
    if (elem == e)
      requester ! ContainsResult(id, !removed)
    else {
      val position = positionOf(e)
      subtrees.get(position).map(_ ! Contains(requester, id, e)).getOrElse {
        requester ! ContainsResult(id, false)
      }
    }
  }

  private def remove(requester: ActorRef, id:Int, e:Int): Unit = {
    if (e == elem){
      removed = true
      requester ! OperationFinished(id)
    }else{
      val position = positionOf(e)
      subtrees.get(position).map(_ ! Remove(requester, id, e)).getOrElse {
        requester ! OperationFinished(id)
      }
    }
      
  }
  
  private def positionOf(e: Int) = if (e < elem) Left else Right
}
